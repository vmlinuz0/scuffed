// libs
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "scuffed.h"
#include "script.h"

#ifdef HAVE_LIMITS_H
#include <limits.h>
#else
#define PATH_MAX 4096 // Define a conservative value
#endif

int main()
{
	typedef short int mode_t;
	// After handing off from kernel, execute this code
	printf("Running init system...\n");
	printf("Dropped to scuffed shell!\nRun 'help' for help.\n");
	char buffer[256];
	char command[256];
	while (1)
	{ 
		char *pwd = getpwd();
		printf("scuffed %s > ", pwd);
		free(pwd);
		fflush(stdout);
		read(0, buffer, 256);
		buffer[strcspn(buffer, "\n")] = '\0';
		if (strlen(buffer) != 0)
		{
			sscanf(buffer, "%s", command); // Use sscanf to parse the first word
			if (strcmp(command, "hello") == 0)
			{
				hello();
			}
			else
			{
				if (strcmp(command, "help") == 0)
				{
					help();
				}
				else
				{
					if (strcmp(command, "ver") == 0)
					{
						version();
					}
					else
					{
						if (strcmp(command, "panic") == 0)
						{
							break;
						}
						else
						{
							if (strcmp(command, "ls") == 0)
							{

								ls("."); // Call edit_file with the extracted filename
							}
							else
							{
								// Check for commands with arguments (e.g., edit)
								if (strcmp(command, "edit") == 0)
								{
									char filename[256];											// Separate buffer for the filename
									int num_words = sscanf(buffer, "%s %s", command, filename); // Parse command and filename
									if (num_words != 2)
									{ // Check if filename was provided
										printf("Usage: edit <filename>\n");
									}
									else
									{
										edit(filename); // Call edit_file with the extracted filename
									}
								}
								else
								{
									if (strcmp(command, "touch") == 0)
									{
										char filename[256];											// Separate buffer for the filename
										int num_words = sscanf(buffer, "%s %s", command, filename); // Parse command and filename
										if (num_words != 2)
										{ // Check if filename was provided
											printf("Usage: touch <filename>\n");
										}
										else
										{
											touch(filename); // Call edit_file with the extracted filename
										}
									}
									else
									{
										if (strcmp(command, "cat") == 0)
										{
											char filename[256];
											int num_words = sscanf(buffer, "%s %s", command, filename);
											if (num_words != 2)
											{
												printf("Usage: cat <filename>\n");
											}
											else
											{
												cat(filename);
											}
										}
										else
										{
											if (strcmp(command, "echo") == 0)
											{
												char text[256];
												int num_words = sscanf(buffer, "%s %s", command, text);
												if (num_words != 2)
												{
													printf("Usage: echo <text>");
												}
												else
												{
													echo(text);
												}
											}
											else
											{
												char function_name[256];
												int num_words = sscanf(buffer, "%s %s", command, function_name); // Parse command and function name

												if (strcmp(command, "exec") == 0)
												{
													if (num_words != 2)
													{
														printf("Usage: exec <function_name>\n");
													}
													else
													{
														if (strcmp(command, "hoi"))
														{
															hoi();
														}
														else
														{
															printf("Function '%s' not found.\n", function_name);
														}
													}
												}

												else
												{
													if (strcmp(command, "cd") == 0)
													{
														char filename[256];
														int num_words = sscanf(buffer, "%s %s", command, filename);
														char *new_pwd = cd(filename);
														if (new_pwd != NULL)
														{
															// Directory change successful, pwd retrieved
															printf("%s\n", new_pwd); // Print the new pwd
														}
														else
														{
															// Directory change failed or getcwd error (handle appropriately)
															printf("Error: Could not change directory.\n");
														}
													}
													else
													{
														if (strcmp(command, "mkdir") == 0)
														{
															// Extract the directory path from the user input (if provided)
															char filename[256];
															int num_words = sscanf(buffer, "%s %s", command, filename);
															if (strlen(filename) == 0)
															{
																printf("Usage: mkdir <directory_name> [mode]\n"); // Inform user of usage
															}
															else
															{
																// Extract permission mode if provided (optional)
																mode_t mode = 0755; // Default permission (user:rwx, group:rx, others:rx)

																int result = mkdir(filename, mode);
																if (result != 0)
																{
																	// Handle mkdir failure (e.g., directory already exists)
																}
															}
														} else {
															printf("Umm. Kapp'n, that command doesn't exist!\n");
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}