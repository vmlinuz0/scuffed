#include	<stdio.h>		// For printf(), FILE, fopen(), fclose(), fgets(), fputs(), perror()
#include	<unistd.h>	// For read(), write(), close()
#include	<stdlib.h>	// For perror()
#include	<dirent.h>	// For DIR, struct dirent, opendir(), readdir(), closedir()
#include	<string.h>	// For strcmp(), strlen()
#include	<fcntl.h>		// For open(), O_RDONLY, O_WRONLY, O_CREAT, O_EXCL
#include	<sys/stat.h>
#include	<sys/types.h>
#ifdef	HAVE_LIMITS_H
#include	<limits.h>
#else
#define	PATH_MAX	4096	// Define a conservative value
#endif

void	echo(const	char	*text)	{
		printf("%s\n",	text);
}

void	hello()	{
		printf("Hello from init.c!\n");
}

const	char	*help_message	=
				"Scuffed OS Help:\n"
				"  *** SYSTEM COMMANDS\n"
				"  * help: Displays this help message\n"	// Add help for the help command
				"  * hello: Prints a Hello message\n"
				"  * ver: Prints version info\n"
				"  * echo <text>: echos all text back to you\n"
				"  *** FILE COMMMANDS\n"
				"  * ls: Lists files and directories\n"
				"  * cat <filename>: Lists contents of file <filename>\n"
				"  * edit <filename>: Opens a file editor for file <filename>\n"
				"  * cd <directory>: Changes to that directory\n"
				"  * touch <file>: Creates a blank file\n"
				"  * mkdir <directory>: Creates a new directory\n"
				"  *** DEVELOPMENT COMMANDS\n"
				"  * exec <script>: execute script named <script> from script.h\n"
				"  * panic: Invokes a kernel panic if booted from iso\n";

void	help()	{
		printf("%s",	help_message);
}

const	char	*ver	=
				"Scuffed OS Version:\n"
				"		* OS Version: 0.0.1b\n"
				"		* Kernel Version: ?.?.?-??-scuffed\n";
void	version()	{
		printf("%s",	ver);
}

int	cat(const	char	*filename)	{
		int	fd;
		char	buffer[512];
		ssize_t	bytes_read;

		fd	=	open(filename,	O_RDONLY);	// Open for reading
		if	(fd	==	-1)	{
				perror("open");
				return	1;
		}

		// Read the file contents in chunks
		while	((bytes_read	=	read(fd,	buffer,	sizeof(buffer)))	>	0)	{
				write(1,	buffer,	bytes_read);	// Write to standard output
		}

		if	(bytes_read	==	-1)	{	// Check for errors during read
				perror("read");
				close(fd);
				return	1;
		}

		close(fd);
		return	0;
}

void	edit(const	char	*filename)	{
		FILE	*fp;
		char	buffer[256];
		char	input[256];

		fp	=	fopen(filename,	"r+");	// Open for reading and writing

		if	(!fp)	{
				perror("fopen");
				return;
		}

		// Read and display file contents
		printf("Existing file contents:\n");
		while	(fgets(buffer,	sizeof(buffer),	fp)	!=	NULL)	{
				printf("%s",	buffer);
		}

		// Editing loop (basic example)
		while	(1)	{
				printf("\nEnter a line (blank line to exit): ");
				if	(fgets(input,	sizeof(input),	stdin)	==	NULL)	{
						perror("fgets");
						break;
				}

				// Check for exit condition (empty line)
				if	(strlen(input)	==	1	&&	input[0]	==	'\n')	{
						break;
				}

				// Write the line to the file (replace with more advanced editing logic)
				fputs(input,	fp);
		}

		// Close the file
		fclose(fp);
		printf("File edited.\n");
}

void	ls(const	char	*path)	{
		DIR	*dir;
		struct	dirent	*entry;
		struct	stat	statbuf;

		// Check if path exists and is a directory
		if	(stat(path,	&statbuf)	==	-1)	{
				perror("stat");
				return;
		}

		if	(!S_ISDIR(statbuf.st_mode))	{
				printf("%s\n",	path);	// Print the filename if it's a file
				return;
		}

		// Open directory for reading
		dir	=	opendir(path);
		if	(!dir)	{
				perror("opendir");
				return;
		}

		printf("Listing files:\n");
		while	((entry	=	readdir(dir))	!=	NULL)	{
				// Skip special entries (".", "..")
				if	(strcmp(entry->d_name,	".")	==	0	||	strcmp(entry->d_name,	"..")	==	0)	{
						continue;
				}
				printf("%s\n",	entry->d_name);
		}

		closedir(dir);
}

int	touch(const	char	*filename)	{
		int	fd;

		fd	=	open(filename,	O_CREAT	|	O_WRONLY	|	O_EXCL,	0644);	// Create new file for writing exclusively
		if	(fd	==	-1)	{
				perror("open");
				return	1;
		}

		printf("File '%s' created.\n",	filename);
		close(fd);
		return	0;
}
char	*cd(const	char	*pathname)	{
		int	result	=	chdir(pathname);
		if	(result	==	0)	{
				// Directory change successful, get current working directory
				static	char	pwd[PATH_MAX];	// Static buffer to store pwd
				if	(getcwd(pwd,	sizeof(pwd))	!=	NULL)	{
						return	pwd;
				}	else	{
						// Handle getcwd error (unlikely, but you can print an error message here)
						return	NULL;
				}
		}	else	{
				// Directory change failed, return NULL
				return	NULL;
		}
}
char	*getcwd_pwd(void)	{
		static	char	pwd[PATH_MAX];	// Static buffer to store pwd
		if	(getcwd(pwd,	sizeof(pwd))	!=	NULL)	{
				return	pwd;
		}	else	{
				// Handle getcwd error (unlikely, but you can print an error message here)
				return	NULL;
		}
}