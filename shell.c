// libs
#include	<fcntl.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<string.h>

#include	"system.h"

#ifdef	HAVE_LIMITS_H
#include	<limits.h>
#else
#define	PATH_MAX	4096	// Define a conservative value
#endif

enum	shell	{
		HELLO,
		HELP,
		VER,
		ECHO,
		LS,
		CAT,
		EDIT,
		CD,
		TOUCH,
		MKDIR,
		PANIC,
};

int	main()	{
  
		// After handing off from kernel, execute this code
		printf("Running init system...\n");
		printf("Dropped to eksh!\nRun 'help' for help.\n");
		char	buffer[256];
		char	command[256];
		char	arg[256];	// Array to store arguments
		while	(1)	{
				char	pwd[PATH_MAX];
				char	*cwd	=	getcwd(pwd,	PATH_MAX);
				printf("eksh %s > ",	cwd);
				fflush(stdout);
				read(0,	buffer,	256);
				buffer[strcspn(buffer,	"\n")]	=	'\0';
				sscanf(buffer,	"%s %[^\n]",	command,	arg);	// Read both command and arguments
				enum	shell	cmd;
				if	(strcmp(command,	"hello")	==	0)	{
						cmd	=	HELLO;
				}	else	if	(strcmp(command,	"help")	==	0)	{
						cmd	=	HELP;
				}	else	if	(strcmp(command,	"ver")	==	0)	{
						cmd	=	VER;
				}	else	if	(strcmp(command,	"echo")	==	0)	{
						cmd	=	ECHO;
				}	else	if	(strcmp(command,	"ls")	==	0)	{
						cmd	=	LS;
				}	else	if	(strcmp(command,	"cat")	==	0)	{
						cmd	=	CAT;
				}	else	if	(strcmp(command,	"edit")	==	0)	{
						cmd	=	EDIT;
				}	else	if	(strcmp(command,	"cd")	==	0)	{
						cmd	=	CD;
				}	else	if	(strcmp(command,	"touch")	==	0)	{
						cmd	=	TOUCH;
				}	else	if	(strcmp(command,	"mkdir")	==	0)	{
						cmd	=	MKDIR;
				}	else	{
						printf("Umm. Kapp'n, that command doesn't exist!\n");
						continue;	// Continue to the next iteration of the loop
				}
				switch	(cmd)	{
				case	HELLO:
						hello();
						break;
				case	HELP:
						help();
						break;
				case	VER:
						version();
						break;
				case	ECHO:
						echo(arg);	// Pass arguments to the echo function
						break;
				case	LS:
						ls();
						break;
				case	CAT:
						cat(arg);	// Pass arguments to the cat function
						break;
				case	EDIT:
						edit(arg);	// Pass arguments to the edit function
						break;
				case	CD:
						cd(arg);	// Pass arguments to the cd function
						break;
				case	TOUCH:
						touch(arg);	// Pass arguments to the touch function
						break;
				case	MKDIR:
						mkdir(arg,	0777);	// Pass arguments to the mkdir function
						break;
				case	PANIC:
						break;
				}
		}
		return	0;
}
