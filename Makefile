all: build
build:
	gcc -o shell.c system.c
	echo init | cpio -o -H newc > init.cpio
	mv init.cpio iso/boot/init.cpio
	grub-mkrescue -o scuffed.iso iso
	echo "Ready to boot"

all: build

init: blargh
	$(CC) -static -o init shell.c system.c
blargh:
	$(CC) -c shell.c
	$(CC) -c system.c
clean:
	rm -f *.o
	rm -f init
	rm -f scuffed.iso
	rm -f iso/boot/init.cpio

build: init
	echo init | cpio -o -H newc > init.cpio
	mv init.cpio iso/boot/init.cpio
	grub-mkrescue -o scuffed.iso iso
	echo "Ready to boot"
test:
	cd .devenv && ./init

