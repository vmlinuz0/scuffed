
void echo(const char *text);
void hello();
void help();
void version();
int cat(const char *filename);
void edit(const char *filename);
int ls();
int touch(const char *filename);
char *cd(const char *pathname);
int mkdir(const char *pathname, mode_t mode);
char *getcwd_pwd(void);